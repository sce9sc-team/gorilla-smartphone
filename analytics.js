veltijs.analyticsAccount={
		google:{staging:'UA-36628516-1',live:''},
		visualize:{staging:{'PIWIK_URL':"visualize.velti.com",
			'IDSITE':"69",
			'EVENTSITEID':"70",
			},
		live:{}}
};

veltijs.getAnalytics ={
	
	//PDF Analytics (when opening a pdf)
	thin_pdf:{cat:"open pdf",act:"click",lbl:"Thin Glass A New Design Reality"},
	product_pdf:{cat:"open pdf",act:"click",lbl:"Corning Gorilla Glass 2 Product Information Sheet"},
	protective_pdf:{cat:"open pdf",act:"click",lbl:"Protective Cover Glass for Portable Display Devices"},
	controlled_pdf:{cat:"open pdf",act:"click",lbl:"Controlled Edge Damage by Dynamic Impact"},
	easy_pdf:{cat:"open pdf",act:"click",lbl:"Easy-to-Clean Surfaces for Mobile Devices"},
	specialty_pdf:{cat:"open pdf",act:"click",lbl:"Specialty GlassA New Design Element in Consumer Electronics"},
	cover_pdf:{cat:"open pdf",act:"click",lbl:"Corning Gorilla Glass for Large Cover Applications"},
	
		
	facebook_follow:{cat:"Follow",act:"click",lbl:"Facebook_Follow"},
	twitter_follow:{cat:"Follow",act:"click",lbl:"Twitter_Follow"},
	youTube_follow:{cat:"Follow",act:"click",lbl:"YouTube_Follow"},	
		
	//Main menu analytics
	home_main_menu:{cat:"main_menu",act:"click",lbl:"home"},
	products_main_menu:{cat:"main_menu",act:"click",lbl:"products"},
	news_main_menu:{cat:"main_menu",act:"click",lbl:"news"},
	innovating_main_menu:{cat:"main_menu",act:"click",lbl:"innovating"},
	video_main_menu:{cat:"main_menu",act:"click",lbl:"video"},
	fun_main_menu:{cat:"main_menu",act:"click",lbl:"fun"},
	faqs_main_menu:{cat:"main_menu",act:"click",lbl:"faqs"},
	praise_main_menu:{cat:"main_menu",act:"click",lbl:"praise"},
	
	//Home Page
	home_fb:{cat:"FacebookShares",act:"click",lbl:"Home"},
	home_tw:{cat:"TwitterShares",act:"click",lbl:"Home"},
	home_sms:{cat:"SmsShares",act:"click",lbl:"Home"},
	home_email:{cat:"EmailShares",act:"click",lbl:"Home"},
		
	//Products Page
	products_fb:{cat:"FacebookShares",act:"click",lbl:"Product - Main Page"},
	products_tw:{cat:"TwitterShares",act:"click",lbl:"Product - Main Page"},
	products_sms:{cat:"SmsShares",act:"click",lbl:"Product - Main Page"},
	products_email:{cat:"EmailShares",act:"click",lbl:"Product - Main Page"},
		

	//Fun with Gorilla Page
	fun_fb:{cat:"FacebookShares",act:"click",lbl:"Fun with Gorilla page"},
	fun_tw:{cat:"TwitterShares",act:"click",lbl:"Fun with Gorilla page"},
	fun_sms:{cat:"SmsShares",act:"click",lbl:"Fun with Gorilla page"},
	fun_email:{cat:"EmailShares",act:"click",lbl:"Fun with Gorilla page"},

	//News and Events Page
	news_fb:{cat:"FacebookShares",act:"click",lbl:"News and Events"},
	news_tw:{cat:"TwitterShares",act:"click",lbl:"News and Events"},
	news_sms:{cat:"SmsShares",act:"click",lbl:"News and Events"},
	news_email:{cat:"EmailShares",act:"click",lbl:"News and Events"},
	
	//Innovating Page - Overview
	over_fb:{cat:"FacebookShares",act:"click",lbl:"Innovating - overview page"},
	over_tw:{cat:"TwitterShares",act:"click",lbl:"Innovating - overview page"},
	over_sms:{cat:"SmsShares",act:"click",lbl:"Innovating - overview page"},
	over_email:{cat:"EmailShares",act:"click",lbl:"Innovating - overview page"},
		
	//Innovating Page - Characteristics
	chara_fb:{cat:"FacebookShares",act:"click",lbl:"Innovating - characteristics page"},
	chara_tw:{cat:"TwitterShares",act:"click",lbl:"Innovating - characteristics page"},
	chara_sms:{cat:"SmsShares",act:"click",lbl:"Innovating - characteristics page"},
	chara_email:{cat:"EmailShares",act:"click",lbl:"Innovating - characteristics page"},
	
	//Innovating Page - Customization
	custo_fb:{cat:"FacebookShares",act:"click",lbl:"Innovating - customization page"},
	custo_tw:{cat:"TwitterShares",act:"click",lbl:"Innovating - customization page"},
	custo_sms:{cat:"SmsShares",act:"click",lbl:"Innovating - customization page"},
	custo_email:{cat:"EmailShares",act:"click",lbl:"Innovating - customization page"},
	
	//Innovating Page - Applications
	appli_fb:{cat:"FacebookShares",act:"click",lbl:"Innovating - applications page"},
	appli_tw:{cat:"TwitterShares",act:"click",lbl:"Innovating - applications page"},
	appli_sms:{cat:"SmsShares",act:"click",lbl:"Innovating - applications page"},
	appli_email:{cat:"EmailShares",act:"click",lbl:"Innovating - applications page"},
	
	//Innovating Page - Literature
	lite_fb:{cat:"FacebookShares",act:"click",lbl:"Innovating - literature page"},
	lite_tw:{cat:"TwitterShares",act:"click",lbl:"Innovating - literature page"},
	lite_sms:{cat:"SmsShares",act:"click",lbl:"Innovating - literature page"},
	lite_email:{cat:"EmailShares",act:"click",lbl:"Innovating - literature page"},
	
	
	
	//Ringtones analytics Preview
	ringtone_hoots:{cat:"Ringtones Previews",act:"Ringtones Preview",lbl:"Hoots & Grunts"},
	ringtone_victor:{cat:"Ringtones Previews",act:"Ringtones Preview",lbl:"Victor's Mix"},
	ringtone_club:{cat:"Ringtones Previews",act:"Ringtones Preview",lbl:"Club Gorilla"},
	ringtone_congo:{cat:"Ringtones Previews",act:"Ringtones Preview",lbl:"Calls from the Congo"},
	
	//Ringtones analytics Downloads
	hootsmp3:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Hoots & Grunts mp3"},
	hootsm4r:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Hoots & Grunts m4r"},	
	victormp3:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Victor's Mix mp3"},
	victorm4r:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Victor's Mix m4r"},	
	clubmp3:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Club Gorilla mp3"},
	clubm4r:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Club Gorilla m4r"},	
	congomp3:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Calls from the Congo mp3"},
	congom4r:{cat:"Ringtones Download",act:"Ringtones Download",lbl:"Calls from the Congo m4r"},
	
	//Faqs Page
	faq_fb:{cat:"FacebookShares",act:"click",lbl:"Faqs page"},
	faq_tw:{cat:"TwitterShares",act:"click",lbl:"Faqs page"},
	faq_sms:{cat:"SmsShares",act:"click",lbl:"Faqs page"},
	faq_email:{cat:"EmailShares",act:"click",lbl:"Faqs page"},
	
	//Praise for Gorilla Page
	praise_fb:{cat:"FacebookShares",act:"click",lbl:"PraiseforGorilla page"},
	praise_tw:{cat:"TwitterShares",act:"click",lbl:"PraiseforGorilla page"},
	praise_sms:{cat:"SmsShares",act:"click",lbl:"PraiseforGorilla page"},
	praise_email:{cat:"EmailShares",act:"click",lbl:"PraiseforGorilla page"},
	
	//Specific all News and Feeds Page
	specificnews_fb:{cat:"FacebookShares",act:"click",lbl:"News Release Article"},
	specificnews_tw:{cat:"TwitterShares",act:"click",lbl:"News Release Article"},
	specificnews_sms:{cat:"SmsShares",act:"click",lbl:"News Release Article"},
	specificnews_email:{cat:"EmailShares",act:"click",lbl:"News Release Article"},
	
	//AllPraise Page
	allpraise_fb:{cat:"FacebookShares",act:"click",lbl:"AllPraise"},
	allpraise_tw:{cat:"TwitterShares",act:"click",lbl:"AllPraise"},
	allpraise_sms:{cat:"SmsShares",act:"click",lbl:"AllPraise"},
	allpraise_email:{cat:"EmailShares",act:"click",lbl:"AllPraise"},
	
	//ProductsFull Page
	productsfull_fb:{cat:"FacebookShares",act:"click",lbl:"Product - Full List"},
	productsfull_tw:{cat:"TwitterShares",act:"click",lbl:"Product - Full List"},
	productsfull_sms:{cat:"SmsShares",act:"click",lbl:"Product - Full List"},
	productsfull_email:{cat:"EmailShares",act:"click",lbl:"Product - Full List"},
	
	
	//***********************Wallpaper Analytics**************************
			
	//coverflow0pic1	
	coverflow0pic1_fb:{cat:"FacebookShares",act:"click",lbl:"Wallpaper - Gorilla face"},
	coverflow0pic1_tw:{cat:"TwitterShares",act:"click",lbl:"Wallpaper - Gorilla face"},
	coverflow0pic1_sms:{cat:"SmsShares",act:"click",lbl:"Wallpaper - Gorilla face"},
	coverflow0pic1_email:{cat:"EmailShares",act:"click",lbl:"Wallpaper - Gorilla face"},
	
	//coverflow0pic2	
	coverflow0pic2_fb:{cat:"FacebookShares",act:"click",lbl:"Wallpaper - Gorilla Kiss"},
	coverflow0pic2_tw:{cat:"TwitterShares",act:"click",lbl:"Wallpaper - Gorilla Kiss"},
	coverflow0pic2_sms:{cat:"SmsShares",act:"click",lbl:"Wallpaper - Gorilla Kiss"},
	coverflow0pic2_email:{cat:"EmailShares",act:"click",lbl:"Wallpaper - Gorilla Kiss"},
	
	//coverflow0pic3	
	coverflow0pic3_fb:{cat:"FacebookShares",act:"click",lbl:"Wallpaper - Gorilla Frame"},
	coverflow0pic3_tw:{cat:"TwitterShares",act:"click",lbl:"Wallpaper - Gorilla Frame"},
	coverflow0pic3_sms:{cat:"SmsShares",act:"click",lbl:"Wallpaper - Gorilla Frame"},
	coverflow0pic3_email:{cat:"EmailShares",act:"click",lbl:"Wallpaper - Gorilla Frame"},
	
	//coverflow0pic4	
	coverflow0pic4_fb:{cat:"FacebookShares",act:"click",lbl:"Wallpaper - Gorilla Banana"},
	coverflow0pic4_tw:{cat:"TwitterShares",act:"click",lbl:"Wallpaper - Gorilla Banana"},
	coverflow0pic4_sms:{cat:"SmsShares",act:"click",lbl:"Wallpaper - Gorilla Banana"},
	coverflow0pic4_email:{cat:"EmailShares",act:"click",lbl:"Wallpaper - Gorilla Banana"},
	
	//coverflow0pic5	
	coverflow0pic5_fb:{cat:"FacebookShares",act:"click",lbl:"Wallpaper - Gorilla Fossey"},
	coverflow0pic5_tw:{cat:"TwitterShares",act:"click",lbl:"Wallpaper - Gorilla Fossey"},
	coverflow0pic5_sms:{cat:"SmsShares",act:"click",lbl:"Wallpaper - Gorilla Fossey"},
	coverflow0pic5_email:{cat:"EmailShares",act:"click",lbl:"Wallpaper - Gorilla Fossey"},
	
		
	//coverflow0pic1	
	coverflow0pic1:{cat:"SpecificWallpaper",act:"Wallpapers",lbl:"Wallpaper - Gorilla face"},
	
	//coverflow0pic2	
	coverflow0pic2:{cat:"SpecificWallpaper",act:"Wallpapers",lbl:"Wallpaper - Gorilla Kiss"},	
	
	//coverflow0pic3	
	coverflow0pic3:{cat:"SpecificWallpaper",act:"Wallpapers",lbl:"Wallpaper - Gorilla Frame"},	
	
	//coverflow0pic4	
	coverflow0pic4:{cat:"SpecificWallpaper",act:"Wallpapers",lbl:"Wallpaper - Gorilla Banana"},	
	
	//coverflow0pic5	
	coverflow0pic5:{cat:"SpecificWallpaper",act:"Wallpapers",lbl:"Wallpaper - Gorilla Fossey"},	
	

	
	//***********************Video Analytics (facebook, twitter, email, sms)**************************
	
	//default page
	videos_fb:{cat:"FacebookShares",act:"click",lbl:"Videos page"},
	videos_tw:{cat:"TwitterShares",act:"click",lbl:"Videos page"},
	videos_email:{cat:"EmailShares",act:"click",lbl:"Videos page"},
	videos_sms:{cat:"SmsShares",act:"click",lbl:"Videos page"},
	
	//victor1_fb	
	victor1_fb:{cat:"FacebookShares",act:"click",lbl:"Channel Surfing video"},
	victor1_tw:{cat:"TwitterShares",act:"click",lbl:"Channel Surfing video"},
	victor1_email:{cat:"EmailShares",act:"click",lbl:"Channel Surfing video"},
	victor1_sms:{cat:"SmsShares",act:"click",lbl:"Channel Surfing video"},
	
	//victor2_fb
	victor2_fb:{cat:"FacebookShares",act:"click",lbl:"Cooking Up Tomorrow's Kitchen video"},
	victor2_tw:{cat:"TwitterShares",act:"click",lbl:"Cooking Up Tomorrow's Kitchen video"},
	victor2_email:{cat:"EmailShares",act:"click",lbl:"Cooking Up Tomorrow's Kitchen video"},
	victor2_sms:{cat:"SmsShares",act:"click",lbl:"Cooking Up Tomorrow's Kitchen video"},
	
	//victor3_fb
	victor3_fb:{cat:"FacebookShares",act:"click",lbl:"King of the Office video"},
	victor3_tw:{cat:"TwitterShares",act:"click",lbl:"King of the Office video"},
	victor3_email:{cat:"EmailShares",act:"click",lbl:"King of the Office video"},
	victor3_sms:{cat:"SmsShares",act:"click",lbl:"King of the Office video"},
	
	
	//demos1_fb
	demos1_fb:{cat:"FacebookShares",act:"click",lbl:"How Corning Tests Gorilla Glass"},
	demos1_tw:{cat:"TwitterShares",act:"click",lbl:"How Corning Tests Gorilla Glass"},
	demos1_email:{cat:"EmailShares",act:"click",lbl:"How Corning Tests Gorilla Glass"},
	demos1_sms:{cat:"SmsShares",act:"click",lbl:"How Corning Tests Gorilla Glass"},
	
	
	//saying1_fb
	saying1_fb:{cat:"FacebookShares",act:"click",lbl:"Gorilla Tough video"},
	saying1_tw:{cat:"TwitterShares",act:"click",lbl:"Gorilla Tough video"},
	saying1_email:{cat:"EmailShares",act:"click",lbl:"Gorilla Tough video"},
	saying1_sms:{cat:"SmsShares",act:"click",lbl:"Gorilla Tough video"},
	
	//saying2_fb
	saying2_fb:{cat:"FacebookShares",act:"click",lbl:"Microsoft Puts Corning Gorilla Glass 2 to the Test"},
	saying2_tw:{cat:"TwitterShares",act:"click",lbl:"Microsoft Puts Corning Gorilla Glass 2 to the Test"},
	saying2_email:{cat:"EmailShares",act:"click",lbl:"Microsoft Puts Corning Gorilla Glass 2 to the Test"},
	saying2_sms:{cat:"SmsShares",act:"click",lbl:"Microsoft Puts Corning Gorilla Glass 2 to the Test"},
	
	
	//brought1_fb
	brought1_fb:{cat:"FacebookShares",act:"click",lbl:"A Day Made of Glass video"},
	brought1_tw:{cat:"TwitterShares",act:"click",lbl:"A Day Made of Glass video"},
	brought1_email:{cat:"EmailShares",act:"click",lbl:"A Day Made of Glass video"},
	brought1_sms:{cat:"SmsShares",act:"click",lbl:"A Day Made of Glass video"},
	
	//brought2_fb
	brought2_fb:{cat:"FacebookShares",act:"click",lbl:"Corning Gorilla Glass for Seamless Elegant Design"},
	brought2_tw:{cat:"TwitterShares",act:"click",lbl:"Corning Gorilla Glass for Seamless Elegant Design"},
	brought2_email:{cat:"EmailShares",act:"click",lbl:"Corning Gorilla Glass for Seamless Elegant Design"},
	brought2_sms:{cat:"SmsShares",act:"click",lbl:"Corning Gorilla Glass for Seamless Elegant Design"},
	
	//brought3_fb
	brought3_fb:{cat:"FacebookShares",act:"click",lbl:"Changing the Way We Think about Glass"},
	brought3_tw:{cat:"TwitterShares",act:"click",lbl:"Changing the Way We Think about Glass"},
	brought3_email:{cat:"EmailShares",act:"click",lbl:"Changing the Way We Think about Glass"},
	brought3_sms:{cat:"SmsShares",act:"click",lbl:"Changing the Way We Think about Glass"},
	
	
	//***********************Video Analytics (clicks on video)**************************	
	
	victor1:{cat:"GorillaVideos",act:"Video Views",lbl:"Channel Surfing"},
	victor2:{cat:"GorillaVideos",act:"Video Views",lbl:"Cooking Up Tomorrow's Kitchen"},
	victor3:{cat:"GorillaVideos",act:"Video Views",lbl:"King of the Office"},
	
	demos1:{cat:"GorillaVideos",act:"Video Views",lbl:"How Corning Tests Gorilla Glass"},
	
	saying1:{cat:"GorillaVideos",act:"Video Views",lbl:"Gorilla Tough"},
	saying2:{cat:"GorillaVideos",act:"Video Views",lbl:"Microsoft Puts Corning Gorilla Glass 2 to the Test"},
	
	
	brought1:{cat:"GorillaVideos",act:"Video Views",lbl:"A Day Made of Glass"},
	brought2:{cat:"GorillaVideos",act:"Video Views",lbl:"Corning Gorilla Glass for Seamless Elegant Design"},
	brought3:{cat:"GorillaVideos",act:"Video Views",lbl:"Changing the Way We Think about Glass"},
		
	
	
	//***********************Product Pages**************************
	
	//Product page - Acer 
	acer_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Acer"},
	acer_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Acer"},
	acer_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Acer"},
	acer_email:{cat:"EmailShares",act:"click",lbl:"Product page - Acer"},
	
	//Product page - Asus
	asus_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Asus"},
	asus_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Asus"},
	asus_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Asus"},
	asus_email:{cat:"EmailShares",act:"click",lbl:"Product page - Asus"},
	
	//Product page - Dell
	dell_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Dell"},
	dell_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Dell"},
	dell_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Dell"},
	dell_email:{cat:"EmailShares",act:"click",lbl:"Product page - Dell"},
	
	//Product page - HP
	hp_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - HP"},
	hp_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - HP"},
	hp_sms:{cat:"SmsShares",act:"click",lbl:"Product page - HP"},
	hp_email:{cat:"EmailShares",act:"click",lbl:"Product page - HP"},
	
	//Product page - HTC
	htc_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - HTC"},
	htc_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - HTC"},
	htc_sms:{cat:"SmsShares",act:"click",lbl:"Product page - HTC"},
	htc_email:{cat:"EmailShares",act:"click",lbl:"Product page - HTC"},
	
	//Product page - Hyundai
	hyundai_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Hyundai"},
	hyundai_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Hyundai"},
	hyundai_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Hyundai"},
	hyundai_email:{cat:"EmailShares",act:"click",lbl:"Product page - Hyundai"},
	
	//Product page - Lenovo
	lenovo_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Lenovo"},
	lenovo_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Lenovo"},
	lenovo_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Lenovo"},
	lenovo_email:{cat:"EmailShares",act:"click",lbl:"Product page - Lenovo"},
	
	//Product page - Motion Computing
	moti_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Motion Computing"},
	moti_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Motion Computing"},
	moti_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Motion Computing"},
	moti_email:{cat:"EmailShares",act:"click",lbl:"Product page - Motion Computing"},
	
	//Product page - Motorola
	moto_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Motorola"},
	moto_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Motorola"},
	moto_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Motorola"},
	moto_email:{cat:"EmailShares",act:"click",lbl:"Product page - Motorola"},
	
	//Product page - NEC
	nec_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - NEC"},
	nec_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - NEC"},
	nec_sms:{cat:"SmsShares",act:"click",lbl:"Product page - NEC"},
	nec_email:{cat:"EmailShares",act:"click",lbl:"Product page - NEC"},
	
	//Product page - Nokia
	nokia_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Nokia"},
	nokia_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Nokia"},
	nokia_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Nokia"},
	nokia_email:{cat:"EmailShares",act:"click",lbl:"Product page - Nokia"},
	
	//Product page - Samsung
	sam_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Samsung"},
	sam_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Samsung"},
	sam_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Samsung"},
	sam_email:{cat:"EmailShares",act:"click",lbl:"Product page - Samsung"},
	
	//Product page - SamsungSUR40
	sur40_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - SamsungSUR40"},
	sur40_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - SamsungSUR40"},
	sur40_sms:{cat:"SmsShares",act:"click",lbl:"Product page - SamsungSUR40"},
	sur40_email:{cat:"EmailShares",act:"click",lbl:"Product page - SamsungSUR40"},
	
	//Product page - Sk Telesys
	sk_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Sk Telesys"},
	sk_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Sk Telesys"},
	sk_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Sk Telesys"},
	sk_email:{cat:"EmailShares",act:"click",lbl:"Product page - Sk Telesys"},
	
	//Product page - Sonim
	sonim_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Sonim"},
	sonim_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Sonim"},
	sonim_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Sonim"},
	sonim_email:{cat:"EmailShares",act:"click",lbl:"Product page - Sonim"},
	
	//Product page - LG
	lg_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - LG"},
	lg_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - LG"},
	lg_sms:{cat:"SmsShares",act:"click",lbl:"Product page - LG"},
	lg_email:{cat:"EmailShares",act:"click",lbl:"Product page - LG"},
	
	//Product page - Sony
	sony_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Sony"},
	sony_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Sony"},
	sony_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Sony"},
	sony_email:{cat:"EmailShares",act:"click",lbl:"Product page - Sony"},
	
	//Product page - Lumigon
	lumigon_fb:{cat:"FacebookShares",act:"click",lbl:"Product page - Lumigon"},
	lumigon_tw:{cat:"TwitterShares",act:"click",lbl:"Product page - Lumigon"},
	lumigon_sms:{cat:"SmsShares",act:"click",lbl:"Product page - Lumigon"},
	lumigon_email:{cat:"EmailShares",act:"click",lbl:"Product page - Lumigon"},
	
	
	//Page Analytics 
	pageAnalytics:{
		home_page:"Home",
		products_page:"Product - Main Page",
		news_page:"News and Events",
		innovating_page:"Innovating page",
		overview_page:"Innovating - overview page",
		characteristics_page:"Innovating - characteristics page",
		customization_page:"Innovating - customization page",
		applications_page:"Innovating - applications page",
		literature_page:"Innovating - literature page",
		videos_page:"Videos page",
		victor_page:"Videos - Victor page",
		demos_page:"Videos - Demos page",
		saying_page:"Videos - Saying page",
		brought_page:"Videos - Brought page",
		fun_page:"Fun with Gorilla page",
		faqs_page:"Faqs page",
		praise_page:"PraiseforGorilla page",
		
		//Product pages
		Acer_page:"Product page - Acer",
		Asus_page:"Product page - Asus",
		Dell_page:"Product page - Dell",
		Htc_page:"Product page - HTC",
		Lg_page:"Product page - LG",
		Motorola_page:"Product page - Motorola",
		Nec_page:"Product page - NEC",
		Nokia_page:"Product page - Nokia",
		Samsung_page:"Product page - Samsung",
		SK_Telesys_page:" Product page - Sk Telesys",
		Sonim_page:"Product page - Sonim",
		Lenovo_page:"Product page - Lenovo",
		Motion_Computing_page:"Product page - Motion Computing",
		Hyundai_page:"Product page - Hyundai",
		Sony_page:"Product page - Sony",
		HP_page:"Product page - HP",
		Lumigon_page:"Product page - Lumigon",
		SamsungSUR40_page:"Product page - SamsungSUR40",
		
		//Dynamic or external pages
		About_page:"About Us",
		Contact_page:"Contact Us",
		ProductsFull_page:"Product - Full List",
		AllNewsAndFeeds_page:"News & Events - View All",
		SpecificNewsAndFeeds_page:"News Release Article",
		GorillaEmail_page:"Gorilla Email page",
		AllPraise_page:"AllPraise",
		PraiseThankYou_page:"Praise Thank You Page",
		GorillaSMS_page:"Gorilla Sms Form",
		VideoPreview_page:"Video Preview page",
		WallpaperPreview_page:"Fun With Gorilla - Wallpaper"
		}	
	
	
};